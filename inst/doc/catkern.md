---
title: "Package 'catkern'"
author: "Elies Ramon"
date: "2019-02-12"
output:
  rmarkdown::html_vignette:
    toc: true
    keep_md: true
vignette: >
  %\VignetteIndexEntry{catkern}
  %\VignetteEngine{knitr::rmarkdown}
  \usepackage[utf8]{inputenc}
---



## Purpose

The **catkern** package implements some useful kernel functions to handle categorical data.

## Installation

In R console:

    install.packages("devtools")  
    devtools::install_bitbucket("elies_ramon/catkern")

## Package Overview

### Main features

- Computes kernel matrices from categorical datasets
- Plots the PCA of categorical datasets
- Permits the presence of ambiguities (i.e. more than one category) in the variables
- Allows for variable weighting
- Delivers the variable importance
- Can perform random sampling of ambiguities

These functions were originally intended to analyze HIV sequence data.

### Functions provided

- cmatrix()
- kmatrix()
- cplot()
- rfweight()
- mixSample()

`cmatrix()` is the core function. Its input is a matrix or dataframe with categorical data, and its output a kernel matrix. Two kernel functions can be chosen: Overlap and Jaccard. The categorical variables can be weighted or not (thus giving equal importance to all of them). An external vector of weights can be provided as an argument; otherwise, weights will be authomatically computed from the Random Forest variable importances.

`kmatrix()` is analogous to cmatrix, but its available kernels are non categorical: the linear and RBF kernels. The input can be numeric or categorical; in the case of categorical input, a one hot encoding is performed. As in `cmatrix`, the variables can be weighted or not. An external vector of weights can be provided as an argument; otherwise, weights will be authomatically computed from the Random Forest variable importances.

`cplot()` plots the kernel PCA of the data. If the target variable is provided, the points will be coloured, allowing the user to check if the individuals that are more similar in their response are also more similar according to the kernel.

`rfweight()` delivers a vector of variable importances. This vector corresponds to the Increase in Node Impurity, a statistic of variable importance computed by the Random Forest method. If needed, a plot of the importances can delivered additionally.

`mixSample()` takes a categorical dataset with ambiguities as input. If the dataset contains ambiguities/mixtures of categories at some point, the function samples one of the category at random. Its output is the sampled dataset.

### Example data

This package contains three categorical datasets with ambiguities: PI, NRTI and NNRTI ([source](https://hivdb.stanford.edu/pages/genopheno.dataset.html)). These datasets contain the protease (PI) and reverse transcriptase (NRTI and NNRTI) sequences of HIV variants, and its corresponding resistance to several drugs. The sequences with insertions, deletions and truncations in the original dataset have been removed.

## Usage

### Sampling a dataset

A dataset can have ambiguous or mixed categories in some positions; for example:

    library("catkern")
    Example <- na.omit(PI[,-c(1:2,4:9)])
    print(Example[1:7,65:80])
    
| P64 | P65 | P66 | P67 | P68 | P69 | P70 | P71 | P72 | P73 | P74 | P75 | P76 | P77 | P78 | P79 |
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
| I   | E   | I   | C   | G   | H   | K   | A   | IV  | G   | T   | V   | L   | V   | G   | P   |
| L   | E   | IV  | C   | G   | H   | K   | V   | I   | S   | T   | V   | L   | V   | G   | P   |
| I   | E   | I   | C   | G   | H   | K   | T   | I   | G   | T   | V   | L   | V   | G   | P   |
| I   | E   | I   | C   | G   | H   | K   | V   | I   | S   | T   | V   | L   | V   | G   | P   |
| I   | E   | I   | C   | G   | H   | K   | A   | I   | G   | T   | V   | L   | I   | G   | P   |
| I   | E   | I   | C   | G   | Q   | K   | AV  | I   | G   | T   | V   | L   | I   | G   | P   |
| V   | E   | I   | C   | G   | H   | K   | I   | I   | T   | T   | V   | L   | V   | G   | AS  |

To take one of the categories at random and obtain a "clean" dataset, do:

    smplExample <- mixSample(dataset = Example, y=1)
    print(smplExample[1:7,65:80])

    
| P64 | P65 | P66 | P67 | P68 | P69 | P70 | P71 | P72 | P73 | P74 | P75 | P76 | P77 | P78 | P79 |
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
| I   | E   | I   | C   | G   | H   | K   | A   | V   | G   | T   | V   | L   | V   | G   | P   |
| L   | E   | V   | C   | G   | H   | K   | V   | I   | S   | T   | V   | L   | V   | G   | P   |
| I   | E   | I   | C   | G   | H   | K   | T   | I   | G   | T   | V   | L   | V   | G   | P   |
| I   | E   | I   | C   | G   | H   | K   | V   | I   | S   | T   | V   | L   | V   | G   | P   |
| I   | E   | I   | C   | G   | H   | K   | A   | I   | G   | T   | V   | L   | I   | G   | P   |
| I   | E   | I   | C   | G   | Q   | K   | V   | I   | G   | T   | V   | L   | I   | G   | P   |
| V   | E   | I   | C   | G   | H   | K   | I   | I   | T   | T   | V   | L   | V   | G   | A   |

where the `y` argument are the columns that don't have to be sampled (in this case, the target variable).

### Computing the variable importance

We can compute a measure of variable importance as:

    ## Convert the sampled dataset from character to factor
    smplExample[,-1] <-  lapply(smplExample[,-1], as.factor) 
    weights <- rfweight(x = smplExample[,-1], y = smplExample[,1],plot=TRUE)
    weights <- weights/sum(weights) ## If we want the relative importances


`rfweight()` returns a vector. If  `plot = TRUE`, a simple plot of the importances will be delivered as well.


### Computing the kernel matrices

There are four kernel functions to chose from: two specific for categorical data (Overlap and Jaccard) and two non categorical kernels (Linear and RBF). In the former, the appropriate **catkern** function is `cmatrix()`; in the latter is `kmatrix()`. In any case, the usage of the two is way similar. 

**Categorical kernels:**

The simpler call to `cmatrix()` is:

    cExample1 <- cmatrix(data = smplExample[,-1], kernel = "o")

There are two mandatory arguments: `data`, the dataset, and `kernel`, which is the desired kernel ("o" for Overlap and "j" for Jaccard). The target variable **must** be absent from the dataset The output is a normalized kernel matrix, of dimensions *n x n*.

The Jaccard kernel has an interesting property: as it is a kernel on sets, it can handle ambiguities/mixtures in the categories. This implies that we do not need to remove them from our dataset, so no information is lost. When there are not ambiguities, the Jaccard kernel is equivalent to Overlap kernel.

    cExample2 <- cmatrix(data = Example[,-1], kernel = "j")


In the previous invocations to `cmatrix()`, all variables had the same weight when computing the kernel matrix. But if we know that some variables are more important than others to the problem at hand, we can weight them. This is done using the `comp` argument. The two possible scenarios are:

· Known importances: If we already have a vector of variable importance, it can be passed as input to `cmatrix()`:

    cExample3 <- cmatrix(data = smplExample[,-1], kernel = "o", comp = "w", coeff = as.vector(weights))

· Unknown importances: `cmatrix()` will call `rfweight()` to obtain the variable importances, and then will use them to weight the variables during the kernel computing.

    cExample4 <- cmatrix(data = smplExample[,-1], kernel = "o", comp = "w", y = smplExample[,1])

Finally, the last argument we can pass to the `cmatrix()` function is the `g` argument. When a value for `g` is provided, the Overlap and the Jaccard kernels computed are: 

*k(x,y) = e <sup> &gamma; · k<sub>c</sub>(x,y)</sup> / e <sup> &gamma; </sup>*

where  *k<sub>c</sub>(x,y)</sup>* are either the Jaccard or Overlap kernels, and `g` (*&gamma;*) is a hyperparameter. This modified Overlap and Jaccard are non linear and analogous to the RBF kernel.

    cExample5 <- cmatrix(data = Example[,-1], kernel = "j", g = 1, y = Example[,1])

**Non categorical kernels:**

`kmatrix()` usage is pretty similar to `cmatrix()`. Main differences are:

- The kernel matrix is not computed by `kmatrix()`  itself, but passed to the **kernlab** package.
- The linear and RBF kernels are intended to operate in numeric data. If `kmatrix()` receives a categorical dataset, this dataset **must** contain factors. Then, `kmatrix()` will perform a one hot encoding (i.e. convert the categorical variables to dummy variables).
- `g` argument is mandatory when RBF kernel is chosen.

An example:

    cExample6 <- kmatrix(data = smplExample[,-1], kernel = "r", g = 0.1, comp = "w", y = smplExample[,1])


### Prediction with the kernel matrices

To use the kernel matrix for prediction, we strongly recommend using the R package **kernlab**. For example:

    library(kernlab)
    ## Training/test indexes
    n <- nrow(cExample2)
    trInd <- sort(sample(n,0.7*n,replace=FALSE))
    teInd <- (1:n)[-trInd]
    trCmatrix <- cExample2[trInd,trInd]
    teCmatrix <- cExample2[teInd,trInd]
    
    ## Train a SVM with our categorical kernel matrix:
    model <- ksvm(trCmatrix,y = Example$ATV[trInd],type="eps-svr",kernel="matrix") 
    
    ## Predict
    teCmatrix <- teCmatrix[,SVindex(model),drop=FALSE]
    teCmatrix <- as.kernelMatrix(teCmatrix)
    predict(model,teCmatrix)

### Visualizing the kernel-PCA of the dataset

The kernel matrix obtained either with `cmatrix` or `kmatrix` can be used to obtain a kernel-PCA plot. This can be useful to visualize our categorical data and to detect outliers. 

In case we have a target/response variable, we can pass it to the function (`y` argument). This information will be used to paint the dots, allowing us to check visually if the sequences that are considered more similar by our kernel function are also similar in regards to the response variable.

    cplot(cExample2)
    cplot(cExample2, y = Example$ATV, col = c("green","yellow","red"))
    
<img src="Example21.png" width="273" /><img src="Example22.png" width="305" />

Also, we can paint with a different color the data with an unknown response value with `na.col`. This can be done either we are using a color spectrum or not:

    # A different drug of the same database, with more NAs.
    drv <- PI[complete.cases(PI[,-c(1:2,4:9)]),]$DRV 
    cplot(cExample2, y = drv, col = "coral", na.col = "grey", legend = FALSE)
    cplot(cExample2, y = drv, col = c("green","yellow","red"), na.col = "grey50")
    
<img src="Example24.png" width="278" /><img src="Example23.png" width="305" />

The matrix containing the PCs, the eigenvalues and the projected data can be obtained using the `kpca` function of **kernlab**:

    
    cExample2 <- as.kernelMatrix(cExample2)
    catPCA <- kpca(cExample2,  kernel = matrix)
    pcv(catPCA) ## All PCs 
    eig(catPCA) ## Eigenvalues
    rotated(catPCA) ## Projection coordinates 

## Appendix

### Package dependencies

**catkern** strongly relies in three R packages: **kernlab**, to perform the kernel-PCA and to compute the linear and RBF kernel; **ggplot2**, to plot the kernel-PCAs, and **randomForest**, to compute the variable importances.
