# R package 'catkern'

-----------------------------

- Current version: 0.1.0
- Author: Elies Ramon
- e-mail: eramongurrea@gmail.com

## Purpose

The **catkern** package implements some useful kernel functions to handle categorical data. These functions were originally intended to analyze HIV sequence data, in the paper [HIV drug resistance prediction with weighted categorical kernel functions](https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-019-2991-2) by E. Ramon et al. (2019)


## Installation

In R console:  
															
`install.packages("devtools")`  
`devtools::install_bitbucket("elies_ramon/catkern")`

## Package Overview

### Main features

- Computes kernel matrices from categorical datasets
- Plots the PCA of categorical datasets
- Permits the presence of ambiguities (i.e. more than one category) in the variables
- Allows for variable weighting
- Delivers the variable importance
- Can perform random sampling of ambiguities

### Functions provided

- cmatrix()
- kmatrix()
- cplot()
- rfweight()
- mixSample()

`cmatrix()` is the core function. Its input is a matrix or dataframe with categorical data, and its output a kernel matrix. Two kernel functions can be chosen: Overlap and Jaccard. The categorical variables can be weighted or not (thus giving equal importance to all of them). An external vector of weights can be provided as an argument; otherwise, weights will be automatically computed from the Random Forest variable importances.

`kmatrix()` is analogous to cmatrix, but its available kernels are non categorical: the linear and RBF kernels. The input can be numeric or categorical; in the case of categorical input, a one hot encoding is performed. As in `cmatrix`, the variables can be weighted or not. An external vector of weights can be provided as an argument; otherwise, weights will be automatically computed from the Random Forest variable importances.

`cplot()` plots the kernel PCA of the data. If the target variable is provided, the points will be colored, allowing the user to check if the individuals that are more similar in their response are also more similar according to the kernel.

`rfweight()` delivers a vector of variable importances. This vector corresponds to the Increase in Node Impurity, a statistic of variable importance computed by the Random Forest method. If needed, a plot of the importances can delivered additionally.

`mixSample()` takes a categorical dataset with ambiguities as input. If the dataset contains ambiguities/mixtures of categories at some point, the function samples one of the category at random. Its output is the sampled dataset.

### Example data

This package contains three categorical datasets with ambiguities: PI, NRTI and NNRTI ([source](https://hivdb.stanford.edu/pages/genopheno.dataset.html)). These datasets contain the protease (PI) and reverse transcriptase (NRTI and NNRTI) sequences of HIV variants, and its corresponding resistance to several drugs. The sequences with insertions, deletions and truncations in the original dataset have been removed. The datasets are lazy loaded and can be used anytime typing `PI`, `NRTI` or `NNRTI`.

## Usage

### Sampling a dataset

A dataset can have ambiguous or mixed categories in some positions; for example:

`library(catkern)`
`Data <- na.omit(PI[,-c(1:2,4:9)])`  
`print(Data[1:7,65:80])`  
    
| P64 | P65 | P66 | P67 | P68 | P69 | P70 | P71 | P72 | P73 | P74 | P75 | P76 | P77 | P78 | P79 |
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
| I   | E   | I   | C   | G   | H   | K   | A   | IV  | G   | T   | V   | L   | V   | G   | P   |
| L   | E   | IV  | C   | G   | H   | K   | V   | I   | S   | T   | V   | L   | V   | G   | P   |
| I   | E   | I   | C   | G   | H   | K   | T   | I   | G   | T   | V   | L   | V   | G   | P   |
| I   | E   | I   | C   | G   | H   | K   | V   | I   | S   | T   | V   | L   | V   | G   | P   |
| I   | E   | I   | C   | G   | H   | K   | A   | I   | G   | T   | V   | L   | I   | G   | P   |
| I   | E   | I   | C   | G   | Q   | K   | AV  | I   | G   | T   | V   | L   | I   | G   | P   |
| V   | E   | I   | C   | G   | H   | K   | I   | I   | T   | T   | V   | L   | V   | G   | AS  |

To take one of the categories at random and obtain a "clean" dataset, do:

`smplData <- mixSample(dataset = Data, y=1)`  
`print(smplData[1:7,65:80])`

    
| P64 | P65 | P66 | P67 | P68 | P69 | P70 | P71 | P72 | P73 | P74 | P75 | P76 | P77 | P78 | P79 |
|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|-----|
| I   | E   | I   | C   | G   | H   | K   | A   | V   | G   | T   | V   | L   | V   | G   | P   |
| L   | E   | V   | C   | G   | H   | K   | V   | I   | S   | T   | V   | L   | V   | G   | P   |
| I   | E   | I   | C   | G   | H   | K   | T   | I   | G   | T   | V   | L   | V   | G   | P   |
| I   | E   | I   | C   | G   | H   | K   | V   | I   | S   | T   | V   | L   | V   | G   | P   |
| I   | E   | I   | C   | G   | H   | K   | A   | I   | G   | T   | V   | L   | I   | G   | P   |
| I   | E   | I   | C   | G   | Q   | K   | V   | I   | G   | T   | V   | L   | I   | G   | P   |
| V   | E   | I   | C   | G   | H   | K   | I   | I   | T   | T   | V   | L   | V   | G   | A   |

where the `y` argument are the columns that don't have to be sampled (in this case, the target variable).

### Computing the variable importance

We can compute a measure of variable importance as:

`## Convert the sampled dataset from character to factor`  
`smplData[,-1] <-  lapply(smplData[,-1], as.factor)`  
`weights <- rfweight(x = smplData[,-1], y = smplData$ATV,plot=TRUE)`  
`weights <- weights/sum(weights) ## If we want the relative importances`  

`rfweight()` returns a vector. If  `plot = TRUE`, a simple plot of the importances will be delivered as well.


### Computing the kernel matrices

There are four kernel functions to chose from: two specific for categorical data (Overlap and Jaccard) and two non categorical kernels (Linear and RBF). In the former, the appropriate **catkern** function is `cmatrix()`; in the latter is `kmatrix()`. In any case, the usage of the two is way similar. 

**Categorical kernels:**

The simpler call to `cmatrix()` is:

`ovrMatrix <- cmatrix(data = smplData[,-1], kernel = "o")`  

There are two mandatory arguments: `data`, the dataset, and `kernel`, which is the desired kernel ("o" for Overlap and "j" for Jaccard). The target variable **must** be absent from the dataset The output is a normalized kernel matrix, of dimensions *n x n*.

The Jaccard kernel has an interesting property: as it is a kernel on sets, it can handle ambiguities/mixtures in the categories. This implies that we do not need to remove them from our dataset, so no information is lost. When there are not ambiguities, the Jaccard kernel is equivalent to Overlap kernel.

`jacMatrix <- cmatrix(data = Data[,-1], kernel = "j")`  

In the previous invocations to `cmatrix()`, all variables had the same weight when computing the kernel matrix. But if we know that some variables are more important than others to the problem at hand, we can weight them. This is done using the `comp` argument. The two possible scenarios are:

- Known importances: If we already have a vector of variable importance, it can be passed as input to `cmatrix()`:

`wOvMatrix <- cmatrix(data = smplData[,-1], kernel = "o", comp = "w", coeff = as.vector(weights))`  

- Unknown importances: `cmatrix()` will call `rfweight()` to obtain the variable importances, and then will use them to weight the variables during the kernel computing.

`wOvMatrix2 <- cmatrix(data = smplData[,-1], kernel = "o", comp = "w", y = smplData$ATV)`  

Finally, the last argument we can pass to the `cmatrix()` function is the `g` argument. When a value for `g` is provided, the Overlap and the Jaccard kernels computed are: *k(x,y) = e^(g * k_c(x,y)) / e^g* , where *k_c(x,y)* are either the Jaccard or Overlap kernels, and *g* (gamma) is a hyperparameter. This modified Overlap and Jaccard are non linear and analogous to the RBF kernel.

`gJacMatrix <- cmatrix(data = Data[,-1], kernel = "j", g = 1, y = Data$ATV)`  

**Non categorical kernels:**

`kmatrix()` usage is pretty similar to `cmatrix()`. Main differences are:

- The kernel matrix is not computed by `kmatrix()`  itself, but passed to the **kernlab** package.
- The linear and RBF kernels are intended to operate in numeric data. If `kmatrix()` receives a categorical dataset, this dataset **must** contain factors. Then, `kmatrix()` will perform a one hot encoding (i.e. convert the categorical variables to dummy variables).
- `g` argument is mandatory when RBF kernel is chosen.

An example:

`rbfMatrix <- kmatrix(data = smplData[,-1], kernel = "r", g = 0.1, comp = "w", y = smplData$ATV)`  


### Prediction with the kernel matrices

To use the kernel matrix for prediction, we strongly recommend using the R package **kernlab**. For example:

`library(kernlab)`  
`## Training/test indexes`  
`n <- nrow(jacMatrix)`  
`trInd <- sort(sample(n,0.7*n,replace=FALSE)) # Training indexes: 70% dataset`  
`teInd <- (1:n)[-trInd] # Test indexes: 30% dataset`  
`trMatrix <- jacMatrix[trInd,trInd] # Training kernel matrix`  
`teMatrix <- jacMatrix[teInd,trInd] # Test kernel matrix`  
`## Train a regression SVM with our kernel matrix:`  
`model <- ksvm(trMatrix,y = Data$ATV[trInd],type="eps-svr",kernel="matrix")`  
`## Predict`  
`teMatrix <- teMatrix[,SVindex(model),drop=FALSE]`  
`teMatrix <- as.kernelMatrix(teMatrix)`  
`predict(model,teMatrix)`  

### Visualizing the kernel-PCA of the dataset

The kernel matrix obtained either with `cmatrix` or `kmatrix` can be used to obtain a kernel-PCA plot. This can be useful to visualize our categorical data and to detect outliers. 

In case we have a target/response variable, we can pass it to the function (`y` argument). This information will be used to paint the dots, allowing us to check visually if the sequences that are considered more similar by our kernel function are also similar in regards to the response variable.

`cplot(jacMatrix)`  
`cplot(jacMatrix, y = Data$ATV, title = "Simple plot", col = c("green","yellow","red"))`  
    
Also, we can paint with a different color the data with an unknown response value with `na.col`. This can be done either we are using a color spectrum or not:

`# A different drug of the same database, with more NAs.`  
`drv <- PI[complete.cases(PI[,-c(1:2,4:9)]),]$DRV `  
`cplot(jacMatrix, y = drv, col = "coral", na.col = "grey", legend = FALSE)`  
`cplot(jacMatrix, y = drv, col = c("green","yellow","red"), na.col = "grey50")`  
    

The matrix containing the PCs, the eigenvalues and the projected data can be obtained using the `kpca` function of **kernlab**:

`jacMatrix <- as.kernelMatrix(jacMatrix)`  
`catPCA <- kpca(jacMatrix,  kernel = matrix)`  
`pcv(catPCA) ## All PCs `  
`eig(catPCA) ## Eigenvalues`  
`rotated(catPCA) ## Projection coordinates `  

## Appendix

### Package dependencies

**catkern** strongly relies in three R packages: **[kernlab](https://cran.r-project.org/web/packages/kernlab/index.html)**, to perform the kernel-PCA and to compute the linear and RBF kernel; **[ggplot2](https://cran.r-project.org/web/packages/ggplot2/index.html)**, to plot the kernel-PCAs, and **[randomForest](https://cran.r-project.org/web/packages/randomForest/index.html)**, to compute the variable importances.
