% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/CatKernelsFunction.R
\name{cmatrix}
\alias{cmatrix}
\title{Compute categorical kernels}
\usage{
cmatrix(data, kernel, g, comp = "m", coeff, y)
}
\arguments{
\item{data}{An \emph{n * d} matrix or data.frame of predictor variables,
containing categorical data (of class character or factor). Target variable
must be absent. Only letters are allowed as category names.}

\item{kernel}{Choose "o" for Overlap kernel or "j" for Jaccard kernel}

\item{g}{Optional hyperparameter. Returns e^g*(KernelMatrix)/e^g.}

\item{comp}{Defaults to "m". This argument indicated how the variables
of the dataset are combined.
\itemize{
  \item "s" gives the same importance to all variables, and delivers an
  unnormalized kernel matrix.
  \item "m" gives the same importance to all variables, and delivers a
  normalized kernel matrix (all elements range between 0 and 1).
  \item "w" weight each variable for its importance, and delivers a
  normalized kernel matrix.
}}

\item{coeff}{If empty, weights will be authomathically computed as the RF increase
in node impurity. Otherwise, coeff must be a vector of weights of length \emph{d}.}

\item{y}{Only needed when coeff = "rf". It is the vector of responses, of
length \emph{n}, needed to compute the RF weights.}
}
\value{
An \emph{n * n} kernel matrix
}
\description{
This function delivers the Overlap or Jaccard kernel matrix for a dataset.
Weighting each variable is allowed.
}
\examples{
## Generate synthetic data (without mixtures)
nCol <- 5
nRow <- 80
l <- nCol*nRow
dataEx1 <- matrix(sample(LETTERS,l,TRUE), nrow = nRow, ncol = nCol)

## Kernel matrix obtained by using Overlap kernel.
cmatrix1 <- cmatrix(dataEx1, kernel = "o", comp = "s")

## Generate synthetic input data (with mixtures)
max.mixt.size <- 4
random.mixt <- function(x)paste(sort(sample(LETTERS,x,FALSE)),sep="",collapse = "")
mixt.percent <- round(l*runif(1, 0, 1),digits=0)
elements <- sample(l, mixt.percent)
dataEx2 <- dataEx1
for(i in elements) dataEx2[i] <- random.mixt(sample(2:max.mixt.size,1))

## Kernel matrix obtained by using Jaccard kernel with g = 1.
cmatrix2 <- cmatrix(dataEx2, kernel = "j", g = 1, comp=  "s")

## Introducing an external vector of weights
w <- runif(nCol, 1, 100)
cmatrix3 <- cmatrix(dataEx1, kernel = "o", comp = "w", coeff = w)

## Jaccard kernel matrix from PI (example data) dataset
PIJacc <- cmatrix(data = PI[,10:ncol(PI)],kernel = "j",comp = "m")

## Same kernel, but weighted using the RF variable importance criteria
wPI <- na.omit(PI)
wPIJacc <- cmatrix(data = wPI[,10:ncol(wPI)],kernel = "j",comp = "w",y = wPI$ATV)

}
