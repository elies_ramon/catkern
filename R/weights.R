#### RF WEIGHTS ####

#' Compute Weights
#'
#' This function delivers the Random Forest variable importance.
#'
#' @param x A data.frame with the predictor variables. In case of categorical data, the dataframe must be of factor class
#' @param y A vector containing the target variable.
#' @param plot If TRUE, a barplot of the relative variable importance is delivered
#' @return The variable importances (weights).
#' @examples
#' wPI <- na.omit(PI)
#' weights <- rfweight(x = wPI[,10:ncol(wPI)],y = wPI[,2])
#' ## Plotting the importances
#' weights <- rfweight(x = wPI[,10:ncol(wPI)],y = wPI[,2],plot = TRUE)
#'
#' @export
#' @importFrom randomForest randomForest
#' @importFrom graphics barplot

rfweight <- function(x,y,plot=FALSE) {
  cat("Computing RF Weights",sep="\n")
  rf.model <- randomForest::randomForest(x, y)
  w <- randomForest::importance(rf.model)
  w <- as.vector(w)
  names(w) <- colnames(x)
  if(plot) {
    graphics::barplot(w, col = "grey", main = "RF Position Importance",
                      xlab = "Variables", ylab = "Increase in Node Impurity")
  }
  return(w)
}
